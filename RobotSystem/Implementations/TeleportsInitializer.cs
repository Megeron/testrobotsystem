﻿using RobotSystem.DataObjects;
using RobotSystem.Interfaces;

namespace RobotSystem.Implementations
{
    public sealed class TeleportsInitializer : ITeleportsInitializer
    {
        public TeleportsPositions InitTeleportsPositions(char[][] map)
        {
            var result = new TeleportsPositions();
            for (int x = 0; x < map.Length; x++)
            {
                for (int y = 0; y < map[x].Length; y++)
                {
                    if (map[x][y] == 'T')
                    {
                        if (!result.HasTeleports)
                        {
                            result.FirstTeleportPos = new Vector2 { X = x, Y = y };
                            result.HasTeleports = true;
                        }
                        else
                        {
                            result.SecondTeleportPos = new Vector2 { X = x, Y = y };                          
                        }
                    }
                }
            }
           
            return result;
        }
    }
}

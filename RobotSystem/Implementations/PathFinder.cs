﻿using RobotSystem.Constants;
using RobotSystem.DataObjects;
using RobotSystem.Interfaces;

namespace RobotSystem.Implementations
{
    public sealed class PathFinder : IPathFinder
    {     

        public RobotState ChooseMoveDirection(char[][] map, RobotState robotState)
        {
            if(CheckPassable(map, robotState.CurrentPos, robotState.CurrentDirection.Vector, robotState.IsBreaker))
            {
                return robotState;
            }

            if(!robotState.IsPrioritiesInverted)
            {
                for(var i = 0; i < DirectionConsts.Priorities.Length; i++)
                {
                    if(CheckPassable(map, robotState.CurrentPos, DirectionConsts.Priorities[i].Vector, robotState.IsBreaker))
                    {
                        robotState.CurrentDirection = DirectionConsts.Priorities[i];
                        return robotState;
                    }
                }
            }
            else
            {
                for (var i = DirectionConsts.Priorities.Length - 1; i > 0 ; i--)
                {
                    if (CheckPassable(map, robotState.CurrentPos, DirectionConsts.Priorities[i].Vector, robotState.IsBreaker))
                    {
                        robotState.CurrentDirection = DirectionConsts.Priorities[i];
                        return robotState;
                    }
                }
            }

            robotState.CurrentDirection = new Direction { Vector = new Vector2(0, 0) };
            return robotState;
        }

        private bool CheckPassable(char[][] map, Vector2 pos, Vector2 direction, bool IsBreaker)
        {
            var newPos = pos + direction;
            var cell = map[newPos.X][newPos.Y];
            if(cell == '#' || (cell == 'X' && !IsBreaker))
            {
                return false;
            }
            return true;
        }
    }
}

﻿using RobotSystem.Constants;
using RobotSystem.DataObjects;
using RobotSystem.Exceptions;
using RobotSystem.Interfaces;
using System.Collections.Generic;

namespace RobotSystem.Implementations
{
    public sealed class StateController : IStateController
    {
        private static readonly Dictionary<char, Direction> DirectionsMods = new Dictionary<char, Direction>();

        static StateController()
        {
            DirectionsMods.Add('S', DirectionConsts.SouthDirection);
            DirectionsMods.Add('E', DirectionConsts.EastDirection);
            DirectionsMods.Add('N', DirectionConsts.NorthDirection);
            DirectionsMods.Add('W', DirectionConsts.WestDirection);
        }
        public RobotState InitStartState(char[][] map)
        {
            for (int x = 0; x < map.Length; x++)
            {
                for (int y = 0; y < map[x].Length; y++)
                {
                    if (map[x][y] == '@')
                    {
                        return new RobotState() { CurrentPos = new Vector2 { X = x, Y = y }, CurrentDirection = DirectionConsts.Priorities[0] };
                    }
                }
            }
            throw new StartPointNotFoundException();
        }

        public RobotState MakeMove(RobotState robotState)
        {
            var newPos = robotState.CurrentPos + robotState.CurrentDirection.Vector;
            robotState.CurrentPos = newPos;
            return robotState;
        }

       public RobotState ApplyModifier(char[][] map, RobotState robotState, TeleportsPositions teleportsPositions)
        {
            var c = map[robotState.CurrentPos.X][robotState.CurrentPos.Y];
            
            switch (c)
            {
                case 'T':
                    if(robotState.CurrentPos == teleportsPositions.FirstTeleportPos)
                    {
                        robotState.CurrentPos = teleportsPositions.SecondTeleportPos;
                    }
                    else
                    {
                        robotState.CurrentPos = teleportsPositions.FirstTeleportPos;
                    }
                    break;
                case 'B':
                    robotState.IsBreaker = !robotState.IsBreaker;
                    break;
                case 'I':
                    robotState.IsPrioritiesInverted = !robotState.IsPrioritiesInverted;
                    break;
                case '$':
                    robotState.IsFinished = true;
                    break;

            }
           
            Direction newDirection;
            if(DirectionsMods.TryGetValue(c, out newDirection))
            {
                robotState.CurrentDirection = newDirection;
            }

            return robotState;
        }
    }
}

﻿using System.Collections.Generic;
using RobotSystem.DataObjects;
using RobotSystem.Interfaces;

namespace RobotSystem.Implementations
{
    public sealed class TestProblemRobotSystem : IRobotSystem
    {
        public string[] DoMoves(char[][] map)
        {
            var stateController = new StateController();
            var teleportsInitializer = new TeleportsInitializer();
            var pathFinder = new PathFinder();
            var robotState = stateController.InitStartState(map);
            var teleportsPositions = teleportsInitializer.InitTeleportsPositions(map);
            var previousStates = new HashSet<RobotState>();
            var result = new List<string>();
         
            do
            {
                previousStates.Add(robotState);
                robotState = pathFinder.ChooseMoveDirection(map, robotState);
                result.Add(robotState.CurrentDirection.Name);
                robotState = stateController.MakeMove(robotState);
                robotState = stateController.ApplyModifier(map, robotState, teleportsPositions);

                if (robotState.IsBreaker && map[robotState.CurrentPos.X][robotState.CurrentPos.Y] == 'X')
                {
                    map[robotState.CurrentPos.X][robotState.CurrentPos.Y] = ' ';
                    previousStates.Clear();
                }

                if (previousStates.Contains(robotState))
                {
                   robotState.IsLooped = true;
                }
            }
            while (!robotState.IsLooped && !robotState.IsFinished);
            return result.ToArray();
        }

    }
}

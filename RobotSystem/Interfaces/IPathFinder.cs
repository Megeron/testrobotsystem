﻿using RobotSystem.DataObjects;

namespace RobotSystem.Interfaces
{
    public interface IPathFinder
    {
        RobotState ChooseMoveDirection(char[][] map, RobotState robotState);       
    }
}

﻿using System;

namespace RobotSystem.Interfaces
{
    public interface IRobotSystem
    {
        String[] DoMoves(Char[][] map);
    }

}

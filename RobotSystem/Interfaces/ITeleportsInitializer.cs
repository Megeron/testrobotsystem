﻿using RobotSystem.DataObjects;

namespace RobotSystem.Interfaces
{
    public interface ITeleportsInitializer
    {
        TeleportsPositions InitTeleportsPositions(char[][] map);
    }
}

﻿using RobotSystem.DataObjects;

namespace RobotSystem.Interfaces
{
    public interface IStateController
    {
        RobotState InitStartState(char[][] map);
        RobotState MakeMove(RobotState robotState);
        RobotState ApplyModifier(char[][] map, RobotState robotState, TeleportsPositions teleportsPositions);
    }
}

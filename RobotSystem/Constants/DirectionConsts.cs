﻿using RobotSystem.DataObjects;

namespace RobotSystem.Constants
{
    public static class DirectionConsts
    {
        public static readonly Direction SouthDirection = new Direction { Name = "SOUTH", Vector = new Vector2(0, 1) };
        public static readonly Direction EastDirection = new Direction { Name = "EAST", Vector = new Vector2(1, 0) };
        public static readonly Direction NorthDirection = new Direction { Name = "NORTH", Vector = new Vector2(0, -1) };
        public static readonly Direction WestDirection = new Direction { Name = "WEST", Vector = new Vector2(-1, 0) };
        
        public static readonly Direction[] Priorities = {
          SouthDirection,
          EastDirection,
          NorthDirection,
          WestDirection };
    }
}

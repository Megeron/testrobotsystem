﻿using System;

namespace RobotSystem.Exceptions
{
    public class StartPointNotFoundException : Exception
    {
        public override string Message
        {
            get
            {
                return "Start point not found, please check input data";
            }
        }
    }
}

﻿namespace RobotSystem.DataObjects
{
    public struct Vector2 
    {
        public int X { get; set; }
        public int Y { get; set; }


        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2)
            {
                var toComp = (Vector2)obj;
                if(toComp.X == X && toComp.Y == Y)
                {
                    return true;
                } 
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked 
            {
                int hash = 17;               
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                return hash;
            }
        }

        public static bool operator ==(Vector2 a, Vector2 b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Vector2 a, Vector2 b)
        {
            return !(a == b);
        }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            var c = new Vector2();
            c.X = a.X + b.X;
            c.Y = a.Y + b.Y;
            return c;
        }
    }
}

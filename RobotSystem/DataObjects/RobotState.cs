﻿namespace RobotSystem.DataObjects
{
    public struct RobotState
    {
        public Vector2 CurrentPos { get; set; }
        public Direction CurrentDirection { get; set; }
        public bool IsBreaker { get; set; }
        public bool IsPrioritiesInverted { get; set; }
        public bool IsLooped { get; set; }
        public bool IsFinished { get; set; }
    }
}

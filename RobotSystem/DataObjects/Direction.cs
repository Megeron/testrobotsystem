﻿namespace RobotSystem.DataObjects
{
    public struct Direction
    {
        public string Name { get; set; }
        public Vector2 Vector { get; set; }
        public bool IsZeroVector
        {
            get
            {
                return (Vector.X == 0 && Vector.Y == 0);
            }
        }
    }
}

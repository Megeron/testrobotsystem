﻿namespace RobotSystem.DataObjects
{
    public struct TeleportsPositions
    {
        public Vector2 FirstTeleportPos { get; set; }
        public Vector2 SecondTeleportPos { get; set; }
        public bool HasTeleports { get; set; }
    }
}

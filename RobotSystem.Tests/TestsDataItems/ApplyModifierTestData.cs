﻿using RobotSystem.DataObjects;

namespace RobotSystem.Tests.TestsDataItems
{
    internal sealed class ApplyModifierTestData
    {
        public RobotState Answer { get; set; }
        public char[][] Map { get; set; }
        public RobotState RobotState { get; set; }
        public TeleportsPositions TeleportsPositions { get; set; }
    }
}

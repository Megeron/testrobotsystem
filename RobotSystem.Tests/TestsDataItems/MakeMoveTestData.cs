﻿using RobotSystem.DataObjects;

namespace RobotSystem.Tests.TestsDataItems
{
    internal sealed class MakeMoveTestData
    {
        public RobotState Answer { get; set; }
        public RobotState RobotState { get; set; }       
    }
}

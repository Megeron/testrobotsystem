﻿using RobotSystem.DataObjects;

namespace RobotSystem.Tests.TestsDataItems
{
    internal sealed class InitTeleportsPositionsTestData
    {
        public TeleportsPositions Answer { get; set; }
        public char[][] Map { get; set; }
    }
}

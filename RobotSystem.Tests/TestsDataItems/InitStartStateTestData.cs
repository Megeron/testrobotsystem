﻿using RobotSystem.DataObjects;

namespace RobotSystem.Tests.TestsDataItems
{
    internal sealed class InitStartStateTestData
    {
        public RobotState Answer { get; set; }
        public char[][] Map { get; set; }
    }
}

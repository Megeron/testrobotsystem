﻿namespace RobotSystem.Tests.TestsDataItems
{
    internal sealed class DoMovesTestData
    {     
        public string[] Answer { get; set; }        
        public char[][] Map { get; set; }     
    }
}

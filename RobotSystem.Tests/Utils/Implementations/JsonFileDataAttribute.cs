﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Xunit.Sdk;

namespace RobotSystem.Tests.Utils.Implementations
{
    internal sealed class JsonFileDataAttribute : DataAttribute
    {
        private readonly string _filePath;
        private readonly Type _dataType;

        public JsonFileDataAttribute(string filePath, Type dataType)
        {
            _filePath = filePath;
            _dataType = dataType;
        }

        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            if (testMethod == null) { throw new ArgumentNullException(nameof(testMethod)); }

            var path = Path.IsPathRooted(_filePath)
                ? _filePath
                : Path.GetFullPath(_filePath);

            if (!File.Exists(path))
            {
                throw new ArgumentException($"Could not find file at path: {path}");
            }

            var fileData = File.ReadAllText(_filePath);
            var tmp = JsonConvert.DeserializeObject<List<object[]>>(fileData);
            var result = new List<object[]>();
            foreach (var array in tmp)
            {
                var deserializedItemsList = new List<object>();
                foreach (var item in array)
                {
                    var jItem = item as JObject;
                    var deserializedItem = jItem.ToObject(_dataType);
                    deserializedItemsList.Add(deserializedItem);
                }
                result.Add(deserializedItemsList.ToArray());
            }
            return result;
        }
    }
}

﻿using RobotSystem.Exceptions;
using RobotSystem.Implementations;
using RobotSystem.Interfaces;
using RobotSystem.Tests.TestsDataItems;
using RobotSystem.Tests.Utils.Implementations;
using System;
using Xunit;

namespace RobotSystem.Tests.SystemsTests
{
    public class RobotSystemTests
    {
        private IRobotSystem _robotSystem = new TestProblemRobotSystem();
        private IPathFinder _pathFinder = new PathFinder();
        private IStateController _stateController = new StateController();
        private ITeleportsInitializer _teleportsInitializer = new TeleportsInitializer();

        [Fact]
        void TestDoMoves_CorrectAnswer_WithoutFile()
        {
            var testData = new DoMovesTestData();
            var rotatedMap = new char[12][];
            rotatedMap[0] =  "###############".ToCharArray();
            rotatedMap[1] =  "#  $   IXXXXX #".ToCharArray();
            rotatedMap[2] =  "#  @          #".ToCharArray();
            rotatedMap[3] =  "#             #".ToCharArray();
            rotatedMap[4] =  "#  I          #".ToCharArray();
            rotatedMap[5] =  "#  B   S     W#".ToCharArray();
            rotatedMap[6] =  "#  B   T      #".ToCharArray();
            rotatedMap[7] =  "#             #".ToCharArray();
            rotatedMap[8] =  "#         T   #".ToCharArray();
            rotatedMap[9] =  "#             #".ToCharArray();
            rotatedMap[10] = "#        XXXX #".ToCharArray();
            rotatedMap[11] = "###############".ToCharArray();

            testData.Map = new char[rotatedMap[0].Length][];
            for(int i = 0; i < rotatedMap[0].Length; i++)
            {
                testData.Map[i] = new char[rotatedMap.Length];
                for(int j = 0; j < rotatedMap.Length; j++)
                {
                    testData.Map[i][j] = rotatedMap[j][i];
                }
            }

            testData.Answer = new[] { "SOUTH", "SOUTH", "SOUTH", "SOUTH", "SOUTH", "SOUTH", "SOUTH", "SOUTH", "WEST",
                "WEST", "NORTH", "NORTH", "NORTH", "NORTH", "NORTH", "NORTH", "NORTH", "NORTH", "NORTH", "EAST", "EAST" };
            
            var result = _robotSystem.DoMoves(testData.Map);
            Assert.Equal(testData.Answer, result);
        }

        [Theory]
        [JsonFileData("DoMoves_CorrectAnswer_Tests.json", typeof(DoMovesTestData))]
        void TestDoMoves_CorrectAnswer(DoMovesTestData testData)
        {
           Assert.Equal(testData.Answer, _robotSystem.DoMoves(testData.Map));
        }

        [Theory]
        [JsonFileData("InitTeleportsPositions_CorrectAnswer_Tests.json", typeof(InitTeleportsPositionsTestData))]
        void TestInitTeleportsPositions_CorrectAnswer(InitTeleportsPositionsTestData testData)
        {
          
            Assert.Equal(testData.Answer, _teleportsInitializer.InitTeleportsPositions(testData.Map));
        }

        [Theory]
        [JsonFileData("ChooseMoveDirection_CorrectAnswer_Tests.json", typeof(ChooseMoveDirectionTestData))]
        void TestChooseMoveDirection_CorrectAnswer(ChooseMoveDirectionTestData testData)
        {
            Assert.Equal(testData.Answer, _pathFinder.ChooseMoveDirection(testData.Map, testData.RobotState));
        }

        [Theory]
        [JsonFileData("InitStartState_CorrectAnswer_Tests.json", typeof(InitStartStateTestData))]
        void TestInitStartState_CorrectAnswer(InitStartStateTestData testData)
        {
            Assert.Equal(testData.Answer, _stateController.InitStartState(testData.Map));
        }

        [Theory]
        [JsonFileData("ApplyModifier_CorrectAnswer_Tests.json", typeof(ApplyModifierTestData))]
        void TestApplyModifier_CorrectAnswer(ApplyModifierTestData testData)
        {
            Assert.Equal(testData.Answer, _stateController.ApplyModifier(testData.Map, testData.RobotState, testData.TeleportsPositions));
        }

        [Theory]
        [JsonFileData("MakeMove_CorrectAnswer_Tests.json", typeof(MakeMoveTestData))]
        void TestMakeMove_CorrectAnswer(MakeMoveTestData testData)
        {
            Assert.Equal(testData.Answer, _stateController.MakeMove(testData.RobotState));
        }

        [Theory]
        [JsonFileData("InitStartState_StartPointNotFoundException_Tests.json", typeof(InitStartStateTestData))]
        void TestInitStartState_StartPointNotFoundException(InitStartStateTestData testData)
        {
           Exception ex = Assert.Throws<StartPointNotFoundException>(() => _stateController.InitStartState(testData.Map));
           Assert.NotNull(ex);
        }


    }
}
